package ch.fhnw.richards.PersonEmail;

import ch.fhnw.richards.PersonEmail.email.Email;
import ch.fhnw.richards.PersonEmail.email.EmailRepository;
import ch.fhnw.richards.PersonEmail.person.Person;
import ch.fhnw.richards.PersonEmail.person.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class LoadDatabase implements CommandLineRunner {
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private EmailRepository emailRepository;

    @Override
    public void run(String... args) throws Exception {
        Person p1 = new Person(1, "Anna", "Aardvark");
        personRepository.save(p1);
        Person p2 = new Person(2, "Beat", "Bungiejumper");
        personRepository.save(p2);

        Email e11 = new Email("anna@greatsky.com", "Great Sky Email");
        e11.setPerson(p1);
        emailRepository.save(e11);
        Email e12 = new Email("anna@patheticocean.com", "Pathetic Ocean Email");
        e12.setPerson(p1);
        emailRepository.save(e12);
        Email e21 = new Email("beat@greatsky.com", "Great Sky Email");
        e21.setPerson(p2);
        emailRepository.save(e21);
        Email e22 = new Email("beat@patheticocean.com", "Pathetic Ocean Email");
        e22.setPerson(p2);
        emailRepository.save(e22);
    }
}
