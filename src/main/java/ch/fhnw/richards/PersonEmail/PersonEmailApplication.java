package ch.fhnw.richards.PersonEmail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PersonEmailApplication {

	public static void main(String[] args) {
		SpringApplication.run(PersonEmailApplication.class, args);
	}

}
