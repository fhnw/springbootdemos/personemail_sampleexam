package ch.fhnw.richards.PersonEmail.person;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PersonController {
    private final PersonRepository repository;

    PersonController(PersonRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/persons")
    List<Person> all() {
        return repository.findAll();
    }

    @GetMapping("/persons/{id}")
    Person one(@PathVariable Integer id) {
        return repository.findById(id)
                .orElseThrow(() -> new PersonNotFoundException(id));
    }
}
