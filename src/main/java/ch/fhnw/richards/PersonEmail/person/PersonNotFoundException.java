package ch.fhnw.richards.PersonEmail.person;

public class PersonNotFoundException extends RuntimeException {
    PersonNotFoundException(Integer id) {
        super("Could not find person " + id);
    }
}
