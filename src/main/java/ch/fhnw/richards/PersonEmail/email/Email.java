package ch.fhnw.richards.PersonEmail.email;

import ch.fhnw.richards.PersonEmail.person.Person;
import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;

import java.util.Objects;

@Entity
@Table(name = "email")
public class Email {
    @Id
    @Column(name = "emailAddress")
    private String emailAddress;
    @Column(name = "provider")
    private String provider;
    @ManyToOne
    @JoinColumn(name = "personID")
    @JsonBackReference
    private Person person;

    public Email() {}

    public Email(String emailAddress, String provider) {
        this.emailAddress = emailAddress;
        this.provider = provider;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Email email = (Email) o;
        return Objects.equals(emailAddress, email.emailAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(emailAddress);
    }
}
